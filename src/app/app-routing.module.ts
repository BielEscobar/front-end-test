import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { AlbumsComponent } from './albums/albums.component';
import { AdminComponent } from './admin/admin.component';
import { AuthService } from './services/auth.service';
import { GuardService } from './services/guard.service';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ListAlbumComponent } from './list-album/list-album.component';
import { VerFotosComponent } from './ver-fotos/ver-fotos.component';

const app_routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'admin', component: AdminComponent, canActivate: [GuardService],
    children: [
      { path: 'users', component: UsersComponent },
      { path: 'albums', component: AlbumsComponent },
      { path: 'edit/:id', component: EditUserComponent },
      { path: 'ver-album/:id', component: ListAlbumComponent },
      { path: 'ver-fotos/:id', component: VerFotosComponent },
    ]
  },
  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(app_routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
