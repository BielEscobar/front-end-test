import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {
  private url: string;
  constructor(
    private _http: HttpClient,
  ) {
    this.url = environment.basePath.url_jt;
  }

  getUsers(): Observable<any> {
    let headers = new HttpHeaders().set('Content-type', 'application/json');
    return this._http.get(this.url + 'users', { headers: headers })
  }

  getAlbumsId(id) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.get(this.url + 'users/' + id + '/albums', { headers: headers })
  }

  getPhotosId(id) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.get(this.url + 'albums/' + id + '/photos', { headers: headers })
  }
  //https://jsonplaceholder.typicode.com/albums/1/photos
  //https://jsonplaceholder.typicode.com/users/1/albums



}
