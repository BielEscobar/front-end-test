import { Injectable } from '@angular/core';
import { User } from '../models/users';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public url: string;

  constructor(
    public _http: HttpClient
  ) {
    this.url = environment.basePath.url_re;
  }

  singUp(usuario: User, gettoken = null): Observable<any> {
    if (gettoken != null) {
      usuario.gettoken = gettoken;
    }
    let params = JSON.stringify(usuario);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.post(this.url + 'login', params, { headers: headers });
  }

}
