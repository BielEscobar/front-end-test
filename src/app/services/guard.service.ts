import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService {

  constructor(
    private _authService: AuthService,
    private _router: Router
  ) { }

  canActivate() {
    let identity = this._authService.getIdentity();
    if (identity) {
      return true;
    } else {
      return false;
    }
  }

}
