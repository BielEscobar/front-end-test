import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public token;

  constructor() { }

  getIdentity() {
    let identity = JSON.parse(localStorage.getItem('identity'));
    if (identity != undefined) {
      return identity;
    } else {
      return null;
    }
  }

  logOut() {
    localStorage.removeItem('identity');
    localStorage.clear();
  }
}
