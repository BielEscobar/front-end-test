import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../models/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url: string;

  constructor(
    private _http: HttpClient,
  ) {
    this.url = environment.basePath.url_re;
  }

  getUsers(): Observable<any> {
    let headers = new HttpHeaders().set('Content-type', 'application/json');
    return this._http.get(this.url + 'users', { headers: headers })
  }

  getUserId(id): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.get(this.url + 'users/' + id, { headers: headers })
  }

  updateUser(id, user: User): Observable<any> {
    let params = JSON.stringify(user);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.put(this.url + 'users/' + id, params, { headers: headers });
  }

  deleteUser(id): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.delete(this.url + 'users/' + id, { headers: headers })
  }

}
