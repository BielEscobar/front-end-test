import { Component, OnInit } from '@angular/core';
import { User } from '../models/users';
import { LoginService } from '../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  public user: User;
  public identity: any;
  public status: string;
  public token;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: LoginService
  ) {
    this.user = new User(0, '', '', '', '', '', '');
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this._userService.singUp(this.user).subscribe(
      response => {
        this.identity = response.token;
        if (!this.identity) {
          this.status = 'error';
        } else {
          localStorage.setItem('identity', JSON.stringify(this.user.email));
          localStorage.setItem('token', this.identity);
          this._router.navigate(['/admin/users']);
        }
      }, error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

}
