import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/users';
import { environment } from '../../environments/environment';
import { Animations } from '../animations/animations';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UserService],
  animations: [Animations]
})
export class UsersComponent implements OnInit {

  public status;
  public usuario: any;
  public url: string;

  constructor(
    private _userService: UserService,
  ) {
    this.url = environment.basePath.url_re;
  }

  ngOnInit(): void {
    this.getUser();
  }

  getUser() {
    this._userService.getUsers().subscribe(
      response => {
        this.usuario = response.data;
        console.log(this.usuario);
      }, error => {
        var errorMessage = <any>error;
        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

  eliminarUsuario(id) {
    this._userService.deleteUser(id).subscribe(
      response => {
        console.log(response);
      }, error => {
        console.log(error);
      }
    )
  }

}
