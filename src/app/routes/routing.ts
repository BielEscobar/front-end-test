import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { LoginComponent } from '../login/login.component';
import { AdminComponent } from '../admin/admin.component';
import { UsersComponent } from '../users/users.component';
import { NgModule } from '@angular/core';
import { AlbumsComponent } from '../albums/albums.component';


const app_routes: Routes = [
    { path: '', component: LoginComponent },
    {
        path: 'admin', component: AdminComponent,
        children: [
            { path: 'users', component: UsersComponent },
            { path: 'albums', component: AlbumsComponent },
        ]
    },
    { path: '**', component: LoginComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(app_routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }