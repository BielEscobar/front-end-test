import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/users';
import { environment } from '../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  public usuario: User;
  public status: string;
  public url: string;
  public title: string;
  public id: number;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    this.usuario = new User(0, '', '', '', '', '', '');
    this.url
  }

  ngOnInit(): void {
    this._route.params.forEach(params => {
      this.id = params['id'];
      this._userService.getUserId(this.id).subscribe(
        response => {
          this.usuario = response.data;
        },
        error => {
          var errorMessage = <any>error;
          console.log(errorMessage);
          if (errorMessage != null) {
            this.status = errorMessage;
          }
        }
      );
    });
  }

  editar(){
    this._userService.updateUser(this.id, this.usuario).subscribe(
      response => {
        this._router.navigate(['/admin/users']);
      }, error => {
        console.log(error);
      }
    )
  }

}
