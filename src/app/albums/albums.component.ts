import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { AlbumsService } from '../services/albums.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css'],
  providers: [AlbumsService],
})
export class AlbumsComponent implements OnInit {

  public status;
  public usuario: any;
  public url: string;

  constructor(
    private _albumService: AlbumsService,
  ) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser() {
    this._albumService.getUsers().subscribe(
      response => {
        this.usuario = response;
        console.log(this.usuario);
      }, error => {
        var errorMessage = <any>error;
        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

}
