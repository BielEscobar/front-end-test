import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { AlbumsService } from '../services/albums.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-album',
  templateUrl: './list-album.component.html',
  styleUrls: ['./list-album.component.css'],
  providers: [AlbumsService],
})
export class ListAlbumComponent implements OnInit {

  public status;
  public albums: any;
  public url: string;
  public id: number;

  constructor(
    private _albumService: AlbumsService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._route.params.forEach(params => {
      this.id = params['id'];
      this._albumService.getAlbumsId(this.id).subscribe(
        response => {
          this.albums = response;
          console.log(this.albums);
        },
        error => {
          var errorMessage = <any>error;
          console.log(errorMessage);
          if (errorMessage != null) {
            this.status = errorMessage;
          }
        }
      );
    });
  }

}
